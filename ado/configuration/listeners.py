from django.db.models import signals
from django.dispatch import receiver
from django.core.cache import cache

from ado.configuration.models import ConfigSettingGroup, CACHE_KEY


@receiver(signals.post_save)
def invalidate_config_settings_cache(sender, *args, **kwargs):
    """
    Invalidate cache for config settings
    """
    if not kwargs.get('raw') and issubclass(sender, ConfigSettingGroup):
        cache.delete(CACHE_KEY)
