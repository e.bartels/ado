import os
from django.apps import AppConfig


class ConfigurationConfig(AppConfig):
    name = 'ado.configuration'
    verbose_name = 'Settings'
    icon = '<i class="icon material-icons">settings</i>'
    path = os.path.dirname(__file__)

    def ready(self):
        from ado.configuration import listeners  # noqa
