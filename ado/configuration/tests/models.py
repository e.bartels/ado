from ado.configuration.models import ConfigSettingGroup, TextSetting, CharSetting


class TestGroup(ConfigSettingGroup):
    title = CharSetting()
    description = TextSetting(help_text="Comma-separated list of keywords")
    keywords = TextSetting(help_text=("A short description for your site"))

    class Meta:
        app_label = 'configuration'
