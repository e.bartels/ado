from django import http
from django.urls import reverse
from ado.customadmin.admin import BaseModelAdmin


class SettingAdmin(BaseModelAdmin):
    icon = '<i class="icon material-icons">settings</i>'
    hide_subtitle = True

    def changelist_view(self, request, extra_context=None):
        opts = self.model._meta

        (instance, created) = self.model.objects.get_or_create()

        url_name = 'admin:{app}_{model}_change'.format(
            app=opts.app_label,
            model=opts.model_name,
        )
        url = reverse(url_name, args=[instance.pk])
        return http.HttpResponseRedirect(url)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
