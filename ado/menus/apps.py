from django.apps import AppConfig


class MenusConfig(AppConfig):
    name = 'ado.menus'
    icon = '<i class="icon material-icons">menu</i>'
    verbose_name = 'Menu'
