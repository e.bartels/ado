from django.urls import path
from ado.examples.artists import views
from ado import menus

urlpatterns = [
    path('', views.index, name='artists-artist-index'),
    path('<str:slug>/', views.view, name='artists-artist-view'),
]

menus.register_url('Artists', name='artists-artist-index')
