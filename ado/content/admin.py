from django.contrib import admin
from django import forms

from ado.customadmin.utils import get_webpack_bundle_media
from ado.customadmin.admin import BaseModelAdminMixin


class ContentItemForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Disable ckeditor field for non-text type ContentItem instances
        # This prevents havaing a bunch of hidden ckeditors initialized in the
        # page, which take up a lot of resources.
        if self.instance and self.instance.type != 'text' and 'text' in self.fields:
            self.fields['text'].widget = forms.Textarea()


class ContentItemsInline(BaseModelAdminMixin, admin.StackedInline):
    """Inline for ContentItem model (images/video/text)"""

    template = "admin/edit_inline/content_item_inline.html"
    classes = ['sortable', 'sortable-field__position']
    extra = 0
    form = ContentItemForm
    verbose_image_fk_fields = ['image']
    verbose_video_fk_fields = ['video']
    html_editor_fields = ['text']

    @property
    def media(self):
        super_media = super().media
        media = get_webpack_bundle_media(
            'content_inlines',
            super_media,
            ['admin/js/jquery.init.js'],
        )
        return media
