import json
from django.test import TestCase
from django.template import Template, RequestContext


class MockUser:
    is_authenticated = True

    def has_module_perms(self, *args, **kwargs):
        return True

    def has_perm(self, *args, **kwargs):
        return True


class MockRequest:
    user = MockUser()
    path_info = '/admin/'


class AdminAppJsonTest(TestCase):
    def test_render(self):
        template_str = """
        {% load customadmin %}
        {% admin_app_json %}
        """
        template = Template(template_str)
        request = MockRequest()
        context = RequestContext(request)
        output = template.render(context)
        data = json.loads(output)

        expected_keys = [
            'admin_index_url',
            'available_apps',
            'has_permission',
            'is_authenticated',
            'opts',
        ]
        for key in expected_keys:
            self.assertTrue(key in data)


class GetAppListTest(TestCase):
    def test_render(self):
        template_str = """
        {% load customadmin %}
        {% get_app_list as app_list %}
        {% for app in app_list %}
        {% for model in app.models %}
        {{model.admin_url}}
        {% endfor %}
        {% endfor %}
        """
        template = Template(template_str)
        request = MockRequest()
        context = RequestContext(request)
        output = template.render(context)
        self.assertTrue(output)
