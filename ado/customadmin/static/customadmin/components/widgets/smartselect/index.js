import domReady from '~/lib/domReady'
import SmartSelect from './SmartSelect'
import SmartMultiSelect from './SmartMultiSelect'
import SmartTagSelect from './SmartTagSelect'


// Components
domReady(() => {
  SmartSelect.init()
  SmartMultiSelect.init()
  SmartTagSelect.init()
})
