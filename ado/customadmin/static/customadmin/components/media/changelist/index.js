import domReady from '~/lib/domReady'
import MediaItemChangeList from '~/components/media/changelist/MediaItemChangeList'

domReady(() => {
  MediaItemChangeList.init()
})
