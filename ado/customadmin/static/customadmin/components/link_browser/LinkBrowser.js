/* eslint jsx-a11y/no-autofocus: 0 */
import * as React from 'react'
import cn from 'classnames'
import { useEffect, useState, useMemo } from 'react'
import fetchUrl from '~/lib/fetchUrl'
import './LinkBrowser.scss'

export const LinkBrowser = () => {
  // Fetch data
  const [data, setData] = useState([])
  const fetchLinks = async () => {
    const params = new URLSearchParams(window.location.search)
    const type = params.get('type') ?? 'link'
    const url = `/customadmin/fetch_object_links?type=${type}`
    const resp = await fetchUrl(url)
    const respData = resp.ok ? await resp.json() : ''
    setData(respData)
  }
  useEffect(() => { fetchLinks() }, [])

  // Filter links
  const [search, setSearch] = useState('')

  const objSearchFilter = (obj) => {
    if (!search) return true
    const searchTerms = search.split(/\s+/).map(term => term.toLowerCase())
    return searchTerms.some(term => obj.title.toLowerCase().indexOf(term) >= 0)
  }

  const apps = useMemo(() => {
    return data.map(app => {
      const objects = app.objects
        .filter(obj => !!obj.url && objSearchFilter(obj))

      return objects.length > 0
        ? {
          ...app,
          objects,
        }
        : null
    }).filter(Boolean)
  }, [data, search])

  // Update search term
  const handleSearchChange = (e) => {
    const target = e.target
    setSearch(target.value.trim())
  }

  // Handle link selection
  const handleLinkClick = (url) => () => {
    const params = new URLSearchParams(window.location.search)
    if (window.opener) {
      window.opener.CKEDITOR.tools.callFunction(params.get('CKEditorFuncNum'), url)
      window.close()
    }
    else {
      console.error('window.opener does not exist. This page needs to be called from CKEditor')
    }
  }

  return (
    <div className="LinkBrowser">
      <p>Choose a link to insert</p>

      <div className="LinkBrowser__search">
        <input type="text" autoFocus name="searchInput" onChange={handleSearchChange} />
        <i className="material-icons icon">search</i>
      </div>

      {apps.map(app => (
        <div key={app.id} className="module">
          <h2>{app.verbose_name_plural}</h2>
          <div className="LinkBrowser__objects">
            {app.objects.map(obj => (
              <button
                key={obj.id}
                type="button"
                className={cn({ hasImage: !!obj.image_url })}
                onClick={handleLinkClick(obj.url)}
              >
                <>
                  {obj.title}
                  {obj.image_url
                    ? (
                      <img src={obj.image_url} alt={obj.title} loading="lazy" />
                    )
                    : null}
                </>
              </button>
            ))}
          </div>
        </div>
      ))}
    </div>
  )
}

export default LinkBrowser
