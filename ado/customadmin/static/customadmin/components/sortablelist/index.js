import domReady from '~/lib/domReady'
import getAdminData from '~/lib/getAdminData'
import { isChangeList } from '~/components/core/ChangeList'
import SortableAdminList from './SortableAdminList'

const addChangelistLink = () => {
  // Add link to the SortableAdminList page
  const opts = getAdminData().opts
  const toolsEl = document.querySelector('.object-tools')
  const liEl = document.createElement('li')
  const href = `${document.location.pathname}organize/`
  liEl.innerHTML = `
    <a href="${href}"><i class="icon material-icons">list</i> Organize ${opts.verbose_name_plural}</a>
  `.trim()
  toolsEl.prepend(liEl)
}

domReady(() => {
  SortableAdminList.init()

  if (isChangeList()) {
    addChangelistLink()
  }
})
