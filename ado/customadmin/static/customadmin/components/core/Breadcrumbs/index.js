/**
 * Fixes up Django's breadcrumbs to make them a little easier to style
 */
const init = () => {
  // get dom element
  const breadcrumbs = document.querySelector('.breadcrumbs')
  if (breadcrumbs) {
    // Find links
    const breadcrumbLinks = Array.from(breadcrumbs.querySelectorAll('a'))

    // Get text nodes, removing separators and whitespace
    const breadcrumbText = Array.from(breadcrumbs.childNodes)
      .filter(node => node.nodeType === Node.TEXT_NODE)
      .map(node => node.nodeValue.replace(/[›\s]+/, ''))
      .filter(text => !!text)

    // clear out all children
    breadcrumbs.innerHTML = ''

    // Add links
    breadcrumbLinks.forEach(link => {
      breadcrumbs.appendChild(link)
    })

    // Add final span with text
    const span = document.createElement('span')
    span.innerHTML = breadcrumbText.join(' ')
    breadcrumbs.appendChild(span)
  }
}


export default {
  init,
}
