/* eslint no-param-reassign: off */

import './SubmitRow.scss'

/**
 * Customizations to Django submit row in change forms
 */
const init = () => {
  // Change default save action
  // Django's default is to save and return to the list, we default to 'save and continue editing'
  const submitRow = document.body.querySelector('.submit-row')
  if (submitRow) {
    const submitInputs = Array.from(submitRow.querySelectorAll('input[type="submit"]'))
    if (submitInputs.length > 1) {
      Array.from(submitInputs).forEach((input, i) => {
        // Remove default class and change text for Save input
        if (input.classList.contains('default')) {
          input.classList.remove('default')
          input.value = 'Save and return'
        }
        // Make 'save and continue...' the default
        if (input.value.trim() === 'Save and continue editing') {
          input.value = 'Save'
          input.classList.add('default')
          input.parentNode.insertBefore(input, input.parentNode.firstChild)
        }
      })
    }
  }
}

export const SubmitRow = {
  init,
}

export default SubmitRow
