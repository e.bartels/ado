import queryString from 'query-string'

/**
 * Enables multiple selections for custom MultiListFilter
 */
export const MultiSelectFilter = (filtersEl) => {
  // find filters with class .multi-filter
  const multiFilters = Array.from(filtersEl.querySelectorAll('.multi-filter'))

  multiFilters.forEach(listEl => {
    const param = listEl.dataset.parameterName

    const links = Array.from(listEl.querySelectorAll('a'))
      .filter((_link, i) => i > 0)
    links.forEach(link => {
      link.addEventListener('click', (e) => {
        e.preventDefault()
        const query = queryString.parse(window.location.search)
        const linkQuery = queryString.parse(queryString.extract(link.href))

        if (link.parentNode.classList.contains('selected')) {
          query[param] = (query[param] || '')
            .split(',')
            .filter(val => val !== linkQuery[param])
            .filter(val => !!val)
            .join(',') || undefined
        }
        else {
          query[param] = [
            ...(query[param] || '').split(',').filter(val => !!val),
            linkQuery[param],
          ].join(',') || undefined
        }

        const qString = queryString.stringify(query, { encode: false })
        const url = `${window.location.pathname}?${qString}`
        window.location = url
      })
    })
  })
}

/**
 * Customizations for changelist filters
 */
export const ChangeListFilters = () => {
  // Find filter container
  const filtersEl = document.getElementById('changelist-filter')
  if (!filtersEl) {
    return
  }

  // Find filter elements
  const filterEls = Array.from(filtersEl.querySelectorAll('details'))
  if (!filterEls.length) {
    return
  }

  // Django filter data
  const filters = JSON.parse(sessionStorage.getItem('django.admin.filtersState')) || {}

  // Set initial state to closed (if more than 1 filter or more than 10 filter choices)
  if (filterEls.length > 1 || filterEls[0].children[1].querySelectorAll('li').length >= 10) {
    filterEls.forEach(filterEl => {
      const filterTitle = filterEl.dataset.filterTitle
      if (filterTitle) {
        const isOpen = filters[filterTitle]
        if (isOpen === undefined) {
          filterEl.children[0].click()
        }
      }
    })
  }

  // Multi select filters
  MultiSelectFilter(filtersEl)
}

export default ChangeListFilters
