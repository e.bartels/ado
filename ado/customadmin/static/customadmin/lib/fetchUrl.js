import getCSRFToken from '~/lib/getCSRFToken'

/**
 * Wrapper around 'fetch'
 */
export const fetchUrl = (url, opts = {}) => {
  // Add django csrf
  const headers = opts.headers || new Headers()
  if (opts.method && !/GET|HEAD/.test(opts.method.toUpperCase())) {
    headers.append('X-CSRFToken', getCSRFToken())
  }

  return fetch(url, {
    ...opts,
    headers,
    credentials: 'same-origin',
  })
}

export default fetchUrl
