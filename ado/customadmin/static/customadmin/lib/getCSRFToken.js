import cookie from 'cookie'

export const getCSRFToken = () => {
  const cookies = cookie.parse(document.cookie)
  return cookies.csrftoken
}

export default getCSRFToken
