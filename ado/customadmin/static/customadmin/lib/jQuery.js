/* global django */
// Make django.jQuery available if we need it
export const jQuery = (window.django && window.django.jQuery)
  ? window.django.jQuery
  : null

export default jQuery
