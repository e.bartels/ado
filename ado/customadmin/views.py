from django.apps import apps
from django.shortcuts import render, get_object_or_404
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.admin.views.decorators import staff_member_required
from django import http
from ado.media.resizer import get_resized


def fetch_url(request):
    """
    Returns the url for a django object

    Example: /admin/fetch_url?model=media.Image&pk=1
    """
    try:
        model_string = request.GET['model']
        pk = request.GET['pk']
    except MultiValueDictKeyError:
        raise http.Http404

    try:
        (app_label, model_name) = model_string.split('.')
    except ValueError:
        raise http.Http404

    model = apps.get_model(app_label, model_name)
    if model is None:
        raise http.Http404

    obj = get_object_or_404(model, pk=pk)

    try:
        url = obj.get_absolute_url()
    except AttributeError:
        raise http.Http404

    return http.HttpResponse(url)


def fetch_object_links(request):
    """
    Returns all possible links that are asscessible from django models via 'get_absolute_url'.
    """
    typ = request.GET.get('type', 'link')
    all_app_configs = [c for c in apps.get_app_configs()]

    data = []
    for app_config in all_app_configs:
        models = [m for m in app_config.get_models() if hasattr(m, 'get_absolute_url')]

        # Filter images only
        if typ == 'images':
            models = [
                m
                for m in models
                if m._meta.app_label == 'media' and m._meta.model_name == 'image'
            ]

        for model in models:
            opt = model._meta
            qs = model.objects.all()
            objects = [
                {
                    'id': o.pk,
                    'title': str(o),
                    'url': o.get_absolute_url(),
                    'image_url': (
                        get_resized(o.get_image(), '200x200').url
                        if hasattr(o, 'get_image')
                        else None
                    ),
                }
                for o in qs
            ]

            data.append(
                {
                    'id': f'{opt.app_label}.{opt.model_name}',
                    'app_label': opt.app_label,
                    'model_name': opt.model_name,
                    'verbose_name': opt.verbose_name.title(),
                    'verbose_name_plural': opt.verbose_name_plural.title(),
                    'objects': objects,
                }
            )

    return http.JsonResponse(data, safe=False)


# link browser for CKEDITOR
@staff_member_required
def link_browser(request):
    """
    A link browser for CKEDITOR
    """
    return render(
        request,
        'customadmin/link_browser.html',
        {
            'is_popup': True,
        },
    )
