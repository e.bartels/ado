from django.urls import path
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path('fetch_url', views.fetch_url),
    path('link_browser/', views.link_browser, name="admin-media-link_browse"),
    path('fetch_object_links', views.fetch_object_links),
    path(
        "password_reset/",
        auth_views.PasswordResetView.as_view(),
        name="admin_password_reset",
    ),
    path(
        "password_reset/done/",
        auth_views.PasswordResetDoneView.as_view(),
        name="password_reset_done",
    ),
    path(
        "reset/<uidb64>/<token>/",
        auth_views.PasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
    path(
        "reset/done/",
        auth_views.PasswordResetCompleteView.as_view(),
        name="password_reset_complete",
    ),
]
