import piexif
from PIL import Image as PILImage
import xml.etree.ElementTree as ET


def get_image_dimensions(filefield):
    """
    Calculates image with and height. This version, unlike Django's:
    * Handles svg images
    * Takes into account orientation from EXIF metadata
    """

    # Sould we close file when we're done
    close = filefield.closed

    # Special handling for svg files
    if filefield.name.lower().endswith('.svg'):
        try:
            if filefield.closed:
                filefield.open()
            file_pos = filefield.tell()
            filefield.seek(0)

            try:
                # svg_contents = filefield.read()
                tree = ET.parse(filefield)
                root = tree.getroot()
                width = int(float(root.get('width') or 0))
                height = int(float(root.get('height') or 0))

                # width or height attrs were not in the svg
                if width == 0 or height == 0:
                    viewBox = root.get('viewBox')
                    if viewBox:
                        width, height = [
                            int(float(val or 0)) for val in viewBox.split()[2:]
                        ]

            except (ValueError, ET.ParseError):
                width = 1
                height = 1

        finally:
            if close:
                filefield.close()
            else:
                filefield.seek(file_pos)

        return abs(width or 1), abs(height or 1)

    # non-svg images
    else:
        try:
            if filefield.closed:
                filefield.open()

            file_pos = filefield.tell()
            filefield.seek(0)

            # Get image dimensions from the image field.
            # This can fail with lzw compressed tiffs:
            # "decoder tiff_lzw not available"
            try:
                width = filefield.width
                height = filefield.height
            except OSError:
                width = None
                height = None

            try:
                image = PILImage.open(filefield)

                # Get width/height if not calculated above
                if not width or not height:
                    width, height = image.size

                # Check exif data to see if orientation needs to be corrected
                # see: http://sylvana.net/jpegcrop/exif_orientation.html
                if 'exif' in image.info:
                    exif = piexif.load(image.info.get('exif'))
                    orientation = exif.get('0th', {}).get(piexif.ImageIFD.Orientation)
                    if orientation and orientation in (5, 6, 7, 8):
                        [width, height] = [height, width]
            except Exception:
                pass

        finally:
            if close:
                filefield.close()
            else:
                filefield.seek(file_pos)

        return abs(width or 1), abs(height or 1)
