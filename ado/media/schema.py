import re
import html
from django.conf import settings
from django.utils.html import strip_tags

import graphene
from graphene_django.types import DjangoObjectType
from graphene_django.converter import convert_django_field

from ado.media import models as media_models
from ado.media import resizer
from ado.media.fields.related import (
    RelatedMediaField,
    RelatedImagesField,
    RelatedVideosField,
    RelatedFilesField,
)

# Resized image settings
IMAGE_SIZES = settings.SCALED_IMAGE_SIZES

RE_SPACE = re.compile(r'(\s|\n|\r)+')


# Media types
class MediaType(graphene.Enum):
    image = 0
    video = 1
    file = 2


class ImageSource:
    def __init__(self, file, width=None, height=None):
        self.file = file
        self.width = width
        self.height = height


class SizedImages(graphene.Scalar):
    @staticmethod
    def serialize(source):
        if not isinstance(source, ImageSource):
            raise ValueError(
                f'SizedImages requires argument to be of type {ImageSource}'
            )

        images = {}
        # for key, geometry in (('medium', '420x9000'),):
        for key, geometry in IMAGE_SIZES:
            crop = 'center' if key.endswith('_crop') else None
            source_size = (
                f'{source.width}x{source.height}'
                if (source.width and source.height)
                else None
            )
            resized = resizer.get_resized(
                source.file,
                geometry,
                crop=crop,
                source_size=source_size,
            )
            images[key] = {
                'url': resized.url,
                'width': resized.width,
                'height': resized.height,
                'crop': bool(resized.kwargs.get('crop')),
            }
        return images


class MediaItemMixin:
    caption_text = graphene.String(required=True)

    def resolve_caption_text(self, info, **kwargs):
        caption = self.caption.replace('>', '> ')
        return html.unescape(RE_SPACE.sub(' ', strip_tags(caption)).strip())


class Image(MediaItemMixin, DjangoObjectType):
    type = graphene.NonNull(MediaType)
    url = graphene.String(required=True)
    images = SizedImages(required=True)

    class Meta:
        name = 'Image'
        model = media_models.Image
        fields = [
            'type',
            'id',
            'uid',
            'title',
            'caption',
            'caption_text',
            'url',
            'width',
            'height',
            'alt_text',
            'images',
        ]

    def resolve_type(self, info, **kwargs):
        return MediaType.image

    def resolve_url(self, info, **kwargs):
        return self.filename.url

    def resolve_images(self, info, **kwargs):
        return ImageSource(self.filename, self.width, self.height)


class Video(MediaItemMixin, DjangoObjectType):
    type = graphene.NonNull(MediaType)
    provider = graphene.String(required=True)
    embed = graphene.String(required=True)
    width = graphene.Int(required=True)
    height = graphene.Int(required=True)
    images = SizedImages(required=True)
    alt_text = graphene.String(required=True)

    class Meta:
        name = 'Video'
        model = media_models.Video
        fields = [
            'type',
            'id',
            'uid',
            'title',
            'caption',
            'caption_text',
            'provider',
            'url',
            'embed',
            'width',
            'height',
            'images',
            'alt_text',
        ]

    def resolve_type(self, info, **kwargs):
        return MediaType.video

    def resolve_provider(self, info, **kwargs):
        return self.provider

    def resolve_embed(self, info, **kwargs):
        return self.embed

    def resolve_width(self, info, **kwargs):
        return self.data['width']

    def resolve_height(self, info, **kwargs):
        return self.data['height']

    def resolve_images(self, info, **kwargs):
        return ImageSource(self.video_image)

    def resolve_alt_text(self, info, **kwargs):
        # Empty altText, mostly to make consistent with Image type
        return ''


class File(MediaItemMixin, DjangoObjectType):
    type = graphene.NonNull(MediaType)
    url = graphene.String(required=True)
    filetype = graphene.String()

    class Meta:
        name = 'File'
        model = media_models.File
        fields = [
            'type',
            'id',
            'uid',
            'title',
            'caption',
            'caption_text',
            'url',
            'filetype',
            'filesize',
        ]

    def resolve_type(self, info, **kwargs):
        return MediaType.file

    def resolve_url(self, info, **kwargs):
        return self.filename.url

    def resolve_filetype(self, info, **kwargs):
        return self.mimetype.split('/')[-1].upper()


class MediaItem(graphene.Union):
    class Meta:
        name = 'MediaItem'
        types = [Image, Video, File]


# Graphene field conversions
@convert_django_field.register(RelatedMediaField)
def convert_related_media_field(field, registry=None):
    return graphene.List(graphene.NonNull(MediaItem), required=True)


@convert_django_field.register(RelatedImagesField)
def convert_related_images_field(field, registry=None):
    return graphene.List(graphene.NonNull(Image), required=True)


@convert_django_field.register(RelatedVideosField)
def convert_related_videos_field(field, registry=None):
    return graphene.List(graphene.NonNull(Video), required=True)


@convert_django_field.register(RelatedFilesField)
def convert_related_files_field(field, registry=None):
    return graphene.List(graphene.NonNull(File), required=True)
