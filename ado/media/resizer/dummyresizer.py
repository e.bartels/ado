from .base import ResizedImage, BaseImageResizer


class DummyResizedImage(ResizedImage):
    def __init__(self, width, height, **kwargs):
        url = f'/media/placeholder/{width}/{height}/'
        super().__init__(url, width, height, **kwargs)


class DummyImageResizer(BaseImageResizer):
    """
    An image resizer that doesn't resize at all (just pretends to)
    """

    def _get_resized(self, source_file, geometry, **kwargs):
        width, height = [int(n) for n in geometry.split('x')]
        return DummyResizedImage(width, height, **kwargs)

    def _delete_resized(self, source_file):
        pass
