import re
import os
from urllib.parse import urlparse
from django.conf import settings
from django.core.checks import register, Error, Warning

from .base import ResizedImage, BaseImageResizer
from .dummyresizer import DummyResizedImage
from .config import IMAGE_RESIZER_USE_DUMMY, DEFAULT_IMAGE_RESIZER


# Map source file extension to resized file extension
# Not all output formats are supported so we decide which file resized image
# extension to use based on the input file extension.
# TODO: Think about using .webp once all browsers support it.
# (I'm looking at you, Safari!)
VALID_EXTENSIONS = {
    '': '.jpg',
    '.': '.jpg',
    '.jpg': '.jpg',
    '.jpeg': '.jpg',
    '.tif': '.jpg',
    '.tiff': '.jpg',
    '.dng': '.jpg',
    '.webp': '.jpg',
    '.png': '.png',
    '.gif': '.png',
}

VALID_SIZES = set(
    [
        '200x200',
        '220x220',
        '300x300',
        '360x300',
        '360x360',
        '360x400',
        '420x420',
        '480x360',
        '720x480',
        '800x800',
        '880x800',
        '880x880',
        '1280x720',
        '1920x1080',
        '1900x1200',
        '1920x1200',
        '2560x1440',
        '3840x2160',
        '4096x2160',
        '4000x4000',
    ]
)


@register()
def check_for_settings(app_configs, **kwargs):
    errors = []
    if app_configs is None or 'ado.media' in [c.name for c in app_configs]:
        if DEFAULT_IMAGE_RESIZER.rsplit('.', 1).pop() == 'LambdaImageResizer':
            if not hasattr(settings, 'IMAGE_RESIZER_LAMBDA_URL'):
                errors.append(
                    Error(
                        '"IMAGE_RESIZER_LAMBDA_URL" setting is required '
                        'to serve resized images.',
                        obj='LambdaImageResizer',
                        hint=(
                            'Add to settings.py, e.g. "IMAGE_RESIZER_LAMBDA_URL '
                            '= https://d1234.cloudfront.net/resized/..."'
                        ),
                    )
                )

            storage = getattr(settings, 'DEFAULT_FILE_STORAGE', '')
            if 's3boto' not in storage:
                errors.append(
                    Warning(
                        'You are not using s3boto or s3boto3 for default file storage.',
                        obj='LambdaImageResizer',
                        hint=(
                            'set DEFAULT_FILE_STORAGE to s3boto or s3boto3 '
                            'from django-storages'
                        ),
                    )
                )
                pass
    return errors


class LambdaImageResizer(BaseImageResizer):
    """
    An image resizer that constructs a url to our custom lambda image resizer.
    https://gitlab.com/artcodeinc/lambda-image-resizer
    """

    def _get_resized(self, source_file, geometry, **kwargs):
        # This is the url we're using for the lambda resizer
        IMAGE_RESIZER_LAMBDA_URL = getattr(settings, 'IMAGE_RESIZER_LAMBDA_URL')

        # Check for crop argument
        # We accept either a "cover" or "inside" argument for crop. The "cover"
        # argument will essientially crop the image using the given dimensions.
        # The "inside" argument will only resize the image.
        crop = 'cover' if kwargs.get('crop', False) else 'inside'
        basename, sourceExt = os.path.splitext(os.path.basename(source_file.name))

        # parse requested width and height
        width, height = [int(n) for n in geometry.split('x')]

        # validate size (must by multiple of 100, or whitelisted)
        if geometry not in VALID_SIZES and (width % 100 != 0 or height % 100 != 0):
            raise ValueError(
                f'"{geometry}" is not a valid size. Width and height must be a multiple'
                ' of 100, or be a whitelisted value.'
            )

        # Because lambda resizer does not itself return the new width/height
        # (since it's url-based), we can calculate the new width & height based
        # on the source size.
        if kwargs.get('source_size'):
            source_width, source_height = [
                int(n) for n in kwargs['source_size'].split('x')
            ]

            # Cropped images
            if crop == 'cover':
                target_width, target_height = (width, height)

                # TODO: Fixme
                # Due to a bug in 'sharp' module, the lambda resizer cannot
                # scale down cropped images properly.
                # see: https://github.com/lovell/sharp/issues/1806
                #
                # Once the bug is fixed, enable the following to calculate the
                # correct scaled and cropped size:
                # if source_width < target_width:
                #     ratio = source_width / target_width
                #     target_width = round(target_width * ratio)
                #     target_height = round(target_height * ratio)
                # if source_height < target_height:
                #     ratio = source_height / target_height
                #     target_height = round(source_height * ratio)
                #     target_width = round(target_width * ratio)

            # Uncropped - calculate scaled size
            else:
                ratio = min(width / source_width, height / source_height)
                target_width = round(source_width * ratio)
                target_height = round(source_height * ratio)
        else:
            target_width, target_height = (width, height)

        # If the original source file is not found, optionally use a dummy image.
        # NOTE: avoid using this for remote storage, like s3boto
        if IMAGE_RESIZER_USE_DUMMY:
            if not source_file.storage.exists(source_file.name):
                return DummyResizedImage(width, height)

        # Choose the resized image file extension (defaulting to .jpg)
        ext = VALID_EXTENSIONS.get(sourceExt.lower(), None)
        if not ext:
            return ResizedImage(
                source_file.storage.url(source_file.name),
                target_width,
                target_height,
                **kwargs,
            )

        # Create the url spec for the lambda resizer
        url = urlparse(IMAGE_RESIZER_LAMBDA_URL)
        path = re.sub(
            r'/+',
            '/',
            '/'.join(
                [
                    url.path,
                    f'/{source_file.name}/{width}x{height}.{crop}.{basename}{ext}',
                ]
            ),
        )
        url = url._replace(path=path).geturl()

        return ResizedImage(
            url,
            target_width,
            target_height,
            **kwargs,
        )

    def _delete_resized(self, source_file):
        # Instead, use S3 lifecycle rule to expire images
        pass
