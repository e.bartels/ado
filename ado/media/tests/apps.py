from django.apps import AppConfig


class MediaConfig(AppConfig):
    name = 'ado.media.tests'
    label = 'mediatests'
