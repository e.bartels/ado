import re
import html
from rest_framework import serializers
from django.conf import settings
from django.utils.html import strip_tags

from ado.media.models import Image, Video, File
from ado.media import resizer


IMAGE_SIZES = settings.SCALED_IMAGE_SIZES

RE_SPACE = re.compile(r'(\s|\n|\r)+')


class MediaItemSerializer(serializers.ModelSerializer):
    type = serializers.SerializerMethodField()
    captionText = serializers.SerializerMethodField()

    def get_type(self, obj):
        return obj.subtype._meta.model_name

    def get_captionText(self, obj):
        caption = obj.caption.replace('>', '> ')
        return html.unescape(RE_SPACE.sub(' ', strip_tags(caption)).strip())


class ImageSerializer(MediaItemSerializer):
    url = serializers.SerializerMethodField()
    filename = serializers.SerializerMethodField()
    resized = serializers.SerializerMethodField()
    ratio = serializers.SerializerMethodField()
    isPortrait = serializers.SerializerMethodField()
    altText = serializers.SerializerMethodField()

    class Meta:
        model = Image
        fields = [
            'id',
            'type',
            'uid',
            'url',
            'filename',
            'title',
            'caption',
            'captionText',
            'altText',
            'width',
            'height',
            'ratio',
            'isPortrait',
            'mimetype',
            'resized',
        ]

    def __init__(self, *args, **kwargs):
        self.sizes = kwargs.pop('sizes', IMAGE_SIZES)
        super().__init__(self, *args, **kwargs)

    def get_url(self, obj):
        return obj.filename.url

    def get_filename(self, obj):
        return obj.filename.name

    def get_ratio(self, obj):
        try:
            return float(obj.width) / float(obj.height)
        except ZeroDivisionError:
            # In some cases the width or height might not have been detected
            # (e.g. for SVG's). In that caes, the value will be 0, so we need
            # to handle a possible zero division.
            return 1

    def get_isPortrait(self, obj):
        return self.get_ratio(obj) < 1

    def get_altText(self, obj):
        return obj.alt_text

    def get_resized(self, obj):
        images = {}
        # for key, size in (('medium', '420x9000'),):
        for key, size in self.sizes:
            crop = 'center' if key.endswith('_crop') else None
            thumb = resizer.get_resized(
                obj.filename,
                size,
                crop=crop,
                source_size=f'{obj.width}x{obj.height}',
            )
            images[key] = {
                'url': thumb.url,
                'width': thumb.width,
                'height': thumb.height,
                'crop': thumb.kwargs.get('crop', None),
            }
        return images


class VideoSerializer(MediaItemSerializer):
    info = serializers.SerializerMethodField()
    resized = serializers.SerializerMethodField()
    width = serializers.SerializerMethodField()
    height = serializers.SerializerMethodField()
    ratio = serializers.SerializerMethodField()
    isPortrait = serializers.SerializerMethodField()
    altText = serializers.SerializerMethodField()

    class Meta:
        model = Video
        fields = [
            'id',
            'type',
            'uid',
            'url',
            'title',
            'caption',
            'captionText',
            'video_image',
            'info',
            'width',
            'height',
            'ratio',
            'isPortrait',
            'altText',
            'resized',
        ]

    def __init__(self, *args, **kwargs):
        self.sizes = kwargs.pop('sizes', IMAGE_SIZES)
        super().__init__(self, *args, **kwargs)

    def get_info(self, obj):
        return obj.info

    def get_width(self, obj):
        return obj.data['width']

    def get_height(self, obj):
        return obj.data['height']

    def get_ratio(self, obj):
        resized = self.get_resized(obj)
        item = list(resized.values())[0]
        return float(item['width']) / float(item['height'])

    def get_isPortrait(self, obj):
        return self.get_ratio(obj) < 1

    def get_altText(self, obj):
        # Empty altText, mostly to make consistent with Image type
        return ''

    def get_resized(self, obj):
        images = {}
        # for key, size in (('medium', '420x9000'),):
        for key, size in self.sizes:
            crop = 'center' if key.endswith('_crop') else None
            thumb = resizer.get_resized(
                obj.video_image,
                size,
                crop=crop,
                source_size=f'{obj.video_image_width}x{obj.video_image_height}',
            )
            images[key] = {
                'url': thumb.url,
                'width': thumb.width,
                'height': thumb.height,
                'crop': thumb.kwargs.get('crop', None),
            }
        return images


class FileSerializer(MediaItemSerializer):
    url = serializers.SerializerMethodField()
    filename = serializers.SerializerMethodField()
    filetype = serializers.SerializerMethodField()
    filesize = serializers.SerializerMethodField()

    class Meta:
        model = File
        fields = [
            'id',
            'type',
            'uid',
            'title',
            'caption',
            'filename',
            'url',
            'mimetype',
            'filetype',
            'filesize',
        ]

    def get_url(self, obj):
        return obj.filename.url

    def get_filename(self, obj):
        return obj.filename.name

    def get_filetype(self, obj):
        return obj.mimetype.split('/')[-1].upper()

    def get_filesize(self, obj):
        return obj.filesize


class MediaSerializer(serializers.Serializer):
    def __init__(self, *args, **kwargs):
        self.sizes = kwargs.pop('sizes', IMAGE_SIZES)
        super().__init__(*args, **kwargs)

    def to_representation(self, instance):
        subtype = instance.subtype
        if isinstance(subtype, Image):
            return ImageSerializer(
                sizes=self.sizes,
                context=self.context,
            ).to_representation(subtype)
        elif isinstance(subtype, Video):
            return VideoSerializer(
                sizes=self.sizes,
                context=self.context,
            ).to_representation(subtype)
        elif isinstance(subtype, File):
            return FileSerializer(
                context=self.context,
            ).to_representation(subtype)
