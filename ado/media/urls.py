from django.urls import path, register_converter
from . import views


class ColorConverter:
    """Path converter for hex color values"""

    regex = '[a-fA-F0-9]{3,6}'

    def to_python(self, value):
        return value

    def to_url(self, value):
        return value


register_converter(ColorConverter, 'color')


urlpatterns = [
    # placeholder images
    path('placeholder/<int:width>/<int:height>/', views.placeholder, name='placeholder-image'),
    path('placeholder/<int:width>/<int:height>/<color:bg>/<color:fg>/', views.placeholder, name='placeholder-image'),

    # JSON data on mediaitems
    path('fetch_mediaitem_info/', views.fetch_mediaitem_info, name="admin-media-fetch_mediaitem_info"),
]
