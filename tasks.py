import os
import importlib
from invoke import task, Collection
from termcolor import colored

import ado
project_path = os.path.dirname(ado.__file__)


# Colors
green = lambda s: colored(s, 'green', attrs=['bold'])
red = lambda s: colored(s, 'red', attrs=['bold'])
yellow = lambda s: colored(s, 'yellow', attrs=['bold'])


def django_setup():
    import django
    django.setup()


# Utils
def _generate_secret_key():
    from django.utils.crypto import get_random_string
    chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
    secret_key = get_random_string(50, chars)
    return secret_key


@task
def generate_secret_key(c):
    """
    Print a random string for use in django SECRET_KEY setting
    """
    print(_generate_secret_key())


# Testing
@task(help={
    'module': 'python module to find tests',
    'settings': 'django settings module',
    'watch': 'run tests when code changes',
    'opts': 'options to pass to django tests',
})
def test(c,
         module='ado',
         settings='ado.config.tests',
         watch=False,
         opts='--traceback --failfast'):
    """
    Run django tests
    """
    django_setup()

    test_command = f'django-admin test --settings={settings} {module} {opts}'

    c.run(test_command, warn=True, echo=True, pty=True)
    if watch:
        module_to_test = importlib.import_module(module.split('.')[0])
        inot_paths = set([
            os.path.dirname(module_to_test.__file__),
            project_path,
        ])
        inot_path_arg = ' '.join(list(inot_paths))
        inot_command = (
            'while true; do '
           f'inotifywait -qr --exclude \'.*[^\.py]$\' -e modify,attrib,close_write,move,create,delete {inot_path_arg} ' # noqa
            '&& clear '
           f'&& {test_command}; done;')

        c.run(inot_command, warn=True, pty=True)


@task(name='publish')
def poetry_publish(c):
    """
    Publish package to gitlab pypi registry
    """
    c.run('poetry publish --repository ado', pty=True)


@task(name='build', default=True, help={
    'publish': 'After build, publish to gitlab package repository',
})
def poetry_build(c, publish=False):
    """
    Build python package using poetry, and optionally publish to gitlab
    """
    c.run('poetry build', pty=True)
    if publish:
        poetry_publish(c)


build_tasks = Collection('build')
build_tasks.add_task(poetry_publish)
build_tasks.add_task(poetry_build)

# root namespace
ns = Collection(
    test,
    generate_secret_key,
    build_tasks,
)
